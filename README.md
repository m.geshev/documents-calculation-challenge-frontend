# Documents Calculation Challenge Frontend



## Installing
```
#clone the project from the repository
git clone https://gitlab.com/m.geshev/documents-calculation-challenge-frontend.git

#install the project from the root folder of the project
Run `npm install`
```

## Configure
```
#configure api url
open src/environments/environment.ts and set apiUrl variable
```

## Serve the application
```
Run `ng serve --open` from the root folder of the project.
```