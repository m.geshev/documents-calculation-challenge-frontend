import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/index';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  /**
   * @type {string}
   */
  private url: string = environment.apiUrl;

  constructor(private http: HttpClient) { }

  /**
   * Get total calculations
   * @param params
   * @returns {Observable<any>}
   */
  public getTotalCalculations(params: any): Observable<any> {
    return this.http.post<any>(this.url, params);
  }
}
