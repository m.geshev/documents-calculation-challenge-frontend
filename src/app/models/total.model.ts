import { Base } from './base.model';

export class Total extends Base {
  /**
   * @type {string}
   */
  vat: string;
  /**
   * @type {string}
   */
  customer: string;
  /**
   * @type {string}
   */
  currency: string;
  /**
   * @type {string}
   */
  total: string;
}
