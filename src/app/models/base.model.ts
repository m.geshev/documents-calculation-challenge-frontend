export class Base {
  /**
   * Object constructor
   */
  constructor(obj: object = {}) {
    Object.assign(this, obj);
  }

  /**
   * Update Object properties
   */
  update(obj: object = {}, reset: boolean = false) {
    if (reset) {
      for (const prop in this) {
        if (this[prop]) {
          delete this[prop];
        }
      }
    }

    Object.assign(this, obj);
  }

  /**
   * Compare object equals to other
   */
  equals(obj: object = {}): boolean {
    return (
      JSON.stringify(this).toLowerCase() === JSON.stringify(obj).toLowerCase()
    );
  }

  /**
   * Has object some property that is undefined (not populated)
   */
  get isPopulated(): boolean {
    for (const prop in this) {
      if (
        this.hasOwnProperty(prop) &&
        this[prop] !== undefined &&
        !(this[prop] instanceof Base)
      ) {
        return true;
      }
    }
    return false;
  }

  /**
   * Has object all properties defined (populated)
   */
  get isFullyPopulated(): boolean {
    for (const prop in this) {
      if (this.hasOwnProperty(prop) && !(this[prop] instanceof Base)) {
        if (this[prop] === undefined) {
          return false;
        }
      }
    }
    return true;
  }
}
