import { Component } from '@angular/core';
import { FormGroup, Validators, FormArray, FormBuilder } from '@angular/forms'
import { ApiService } from './services/api.service';
import { Total } from './models/total.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'Documents calculation challenge';

  /**
   * @type {Total[]}
   */
  totals: Total[];

  /**
   * @type {FormGroup}
   */
  dataForm: FormGroup;

  /**
   * @type any
   */
  errors: any

  fileToUpload: File | null = null;

  constructor(
    private fb: FormBuilder,
    private apiService: ApiService,
  ) {
    this.dataForm = this.fb.group({
      file: ['', Validators.required],
      currency: ['', Validators.required],
      currencies: this.fb.array([]),
      vat: '',
    });

  }

  ngOnInit() {
    this.addCurrency();
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  get currencies(): FormArray {
    return this.dataForm.get("currencies") as FormArray
  }

  newCurrency(): FormGroup {
    return this.fb.group({
      name: ['', Validators.required],
      rate: ['', Validators.required],
    })
  }

  addCurrency() {
    this.currencies.push(this.newCurrency());
  }

  removeCurrency(i: number) {
    this.currencies.removeAt(i);
  }

  onSubmit() {
    if (this.dataForm.invalid) {
      return;
    }

    this.errors = null;

    const formData: FormData = new FormData();
    if (this.fileToUpload) {
      formData.append('data', this.fileToUpload, this.fileToUpload.name);
    }
    formData.append('currency', this.dataForm.value.currency);
    for (var i = 0; i < this.dataForm.value.currencies.length; i++) {
      formData.append('exchange_rates[' + i + '][currency]', this.dataForm.value.currencies[i].name);
      formData.append('exchange_rates[' + i + '][rate]', this.dataForm.value.currencies[i].rate);
    }
    formData.append('vat', this.dataForm.value.vat);

    this.apiService
      .getTotalCalculations(formData)
      .subscribe(
        (result: any) => {
          this.totals = [];
          for (const data of result) {
            this.totals.push(new Total(data));
          }
        },
        (result: any) => {
          this.errors = result.error
        },
        () => {
        }
      );
  }

}